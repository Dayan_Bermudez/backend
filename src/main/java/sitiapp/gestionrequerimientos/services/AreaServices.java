package sitiapp.gestionrequerimientos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.Area;
import sitiapp.gestionrequerimientos.repository.AreaRepository;

import java.util.List;

@Service
public class AreaServices {

    @Autowired
    private AreaRepository areaRepository;

    public List<Area> findAll(){
        return areaRepository.findAll();
    }
}
