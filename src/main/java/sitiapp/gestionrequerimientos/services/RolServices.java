package sitiapp.gestionrequerimientos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.repository.RolRepository;

import java.util.List;

@Service
public class RolServices {

    @Autowired
    private RolRepository rolRepository;

    public List<Rol> findAll(){
        return rolRepository.findAll();
    }
}
