package sitiapp.gestionrequerimientos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.Area;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.model.TipoIdentificacion;
import sitiapp.gestionrequerimientos.model.Usuario;
import sitiapp.gestionrequerimientos.repository.UsuarioRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<Usuario> findAll() {
        return usuarioRepository.findAll();
    }

    public <S extends Usuario> S save(S entity) {
        return usuarioRepository.save(entity);
    }

    public Optional<Usuario> findById(Integer id) {
        return usuarioRepository.findById(id);
    }

    public List<Usuario> findAllByNombreLike(String nombre) {
        return usuarioRepository.findAllByNombreLike(nombre);
    }

    public List<Usuario> findAllBySegundoNombreLike(String segundoNombre) {
        return usuarioRepository.findAllBySegundoNombreLike(segundoNombre);
    }

    public List<Usuario> findAllByApellidoLike(String apellido) {
        return usuarioRepository.findAllByApellidoLike(apellido);
    }

    public List<Usuario> findAllBySegundoApellidoLike(String segundoApellido) {
        return usuarioRepository.findAllBySegundoApellidoLike(segundoApellido);
    }

    public List<Usuario> findAllByIdentificacionLike(String identificacion) {
        return usuarioRepository.findAllByIdentificacionLike(identificacion);
    }

    public Optional<Usuario> findByCorreoElectronicoLike(String correo) {
        return usuarioRepository.findByCorreoElectronicoLike(correo);
    }

    public List<Usuario> findAllByCorreoElectronicoLike(String correo){
        return usuarioRepository.findAllByCorreoElectronicoLike(correo);
    }

    public List<Usuario> findAllByEstado(Integer estado) {
        return usuarioRepository.findAllByEstado(estado);
    }

    public List<Usuario> findByIdTipoIdentificacion(Integer id_tipo) {
        return usuarioRepository.findByIdTipoIdentificacion(id_tipo);
    }

    public Integer countByNombreAndApellido(String nombre,String apellido){
        return usuarioRepository.countByNombreAndApellido(nombre,apellido);
    }

    public boolean existsByIdentificacion(Integer identificacion) {
        return usuarioRepository.existsByIdentificacion(identificacion);
    }

    public Integer updateUsuario(Integer id, String nombre, TipoIdentificacion tipoIdentificacion, Integer identificacion, String correoElectronico, Area area, Date fechaDeEdicion, String segundoNombre, String apellido, String segundoApellido, Rol rol) {
        return usuarioRepository.updateUsuario(id,nombre,tipoIdentificacion,identificacion,correoElectronico,
                area, fechaDeEdicion,segundoNombre,apellido,segundoApellido,rol);
    }

    public Integer deleteUsuario(Integer id_usuario) {
        return usuarioRepository.deleteUsuario(id_usuario);
    }

    public Integer getMaxId(){
        return usuarioRepository.getMaxId();
    }
}
