package sitiapp.gestionrequerimientos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.viewModel.RequerimientosDeUsuariosViewModel;
import sitiapp.gestionrequerimientos.repository.RequerimientoDeUsuarioRepository;

import java.util.List;

@Service
public class RequerimientosDeUsuarioService {

    @Autowired
    private RequerimientoDeUsuarioRepository requerimientoDeUsuarioRepository;

    public List<RequerimientosDeUsuariosViewModel> findAll(){
        return requerimientoDeUsuarioRepository.findAll();
    }
}
