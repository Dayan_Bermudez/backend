package sitiapp.gestionrequerimientos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.EstadoRequerimiento;
import sitiapp.gestionrequerimientos.model.Requerimiento;
import sitiapp.gestionrequerimientos.model.Usuario;
import sitiapp.gestionrequerimientos.repository.RequerimientoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class RequerimientoService {

    @Autowired
    private RequerimientoRepository requerimientoRepository;

    public List<Requerimiento> findAll() {
        return requerimientoRepository.findAll();
    }

    public <S extends Requerimiento> S save(S entity) {
        return requerimientoRepository.save(entity);
    }

    public Optional<Requerimiento> findById(Integer id_requerimiento) {
        return requerimientoRepository.findById(id_requerimiento);
    }

    public List<Requerimiento> findAllByEmpleadoId(Integer id_empleado) {
        return requerimientoRepository.findAllByEmpleadoId(id_empleado);
    }

    public Integer updateRequerimiento(Integer id_requerimiento, String descripcion, Usuario empleado, EstadoRequerimiento estado) {
        return requerimientoRepository.updateRequerimiento(id_requerimiento,descripcion,empleado,estado);
    }

    public Integer updateEstadoDelRequerimiento(Integer id_requerimiento, Integer estado) {
        return requerimientoRepository.updateEstadoDelRequerimiento(id_requerimiento,estado);
    }

    public Integer updateEmpleadoDelRequerimiento(Integer id_requerimiento, Integer empleado) {
        return requerimientoRepository.updateEmpleadoDelRequerimiento(id_requerimiento,empleado);
    }

    public Integer deleteRequerimiento(Integer id_requerimiento) {
        return requerimientoRepository.deleteRequerimiento(id_requerimiento);
    }
}
