package sitiapp.gestionrequerimientos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.TipoIdentificacion;
import sitiapp.gestionrequerimientos.repository.TipoIdentificacionRepository;

import java.util.List;

@Service
public class TipoIdentificacionService {

    @Autowired
    private TipoIdentificacionRepository tipoIdentificacionRepository;

    public List<TipoIdentificacion> findAll(){
        return tipoIdentificacionRepository.findAll();
    }
}
