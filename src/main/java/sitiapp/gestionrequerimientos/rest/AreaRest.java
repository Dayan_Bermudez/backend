package sitiapp.gestionrequerimientos.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import javax.validation.constraints.Null;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sitiapp.gestionrequerimientos.model.Area;
import sitiapp.gestionrequerimientos.model.Respuesta;
import sitiapp.gestionrequerimientos.services.AreaServices;

@RestController
@RequestMapping("/area")
@Api("El servicio responde a todas las peticiones relacionadas al modelo Area")
public class AreaRest {

    private final static Logger logger = LoggerFactory.getLogger(AreaRest.class);

    @Autowired
    private AreaServices areaServices;

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todas las Area")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Las Area se han obtenido exitosamente",responseContainer = "List",response = Area.class),
            @ApiResponse(code = 400,message = "error en el metodo getAreas:",response = Null.class)
    })
    private ResponseEntity<List<Area>> getAreas(){
        List<Area> areas = null;
        try{
            areas = areaServices.findAll();
        }catch (Exception exception){
            logger.error("error en el metodo getAreas: "+exception.getMessage());
            return new ResponseEntity(null,HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(areas);
    }
}
