package sitiapp.gestionrequerimientos.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import javax.validation.constraints.Null;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sitiapp.gestionrequerimientos.model.Respuesta;
import sitiapp.gestionrequerimientos.model.TipoIdentificacion;
import sitiapp.gestionrequerimientos.services.TipoIdentificacionService;

@RestController
@RequestMapping("/tipos/identificacion")
@Api("El servicio responde a todas las peticiones relacionadas al modelo TipoIdentificacion")
public class TipoIdentificacionRest {

    private final static Logger logger = LoggerFactory.getLogger(TipoIdentificacionRest.class);

    @Autowired
    private TipoIdentificacionService tipoIdentificacionService;

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todos los TipoIdentificacion")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Los TipoIdentificacion se han obtenidos exitosamente",responseContainer = "List",response = TipoIdentificacion.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuarios:",response = Null.class)
    })
    private ResponseEntity<List<TipoIdentificacion>> getTiposIdentificacion(){
        List<TipoIdentificacion> tipos = null;
        try{
            tipos = tipoIdentificacionService.findAll();
        }catch (Exception exception){
            logger.error("error en el metodo getTipoIdentificacion: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(tipos);
    }
}
