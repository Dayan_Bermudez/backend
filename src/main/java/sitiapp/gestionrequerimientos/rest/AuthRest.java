package sitiapp.gestionrequerimientos.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sitiapp.gestionrequerimientos.model.Respuesta;
import sitiapp.gestionrequerimientos.security.dto.JwtDto;
import sitiapp.gestionrequerimientos.security.dto.LoginUsuario;
import sitiapp.gestionrequerimientos.security.jwt.JwtProvider;
import sitiapp.gestionrequerimientos.services.UsuarioService;

import javax.validation.Valid;
import javax.validation.constraints.Null;

@RestController
@RequestMapping("/auth")
@CrossOrigin
@Api("Servicio de autenticacion")
public class AuthRest {

    private final static Logger logger = LoggerFactory.getLogger(AuthRest.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping(value = "/login",produces = "application/json")
    @ApiOperation(value = "Valida los datos de autenticacion del Usuario y" +
            " Genera el token de la sesion")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuario autenticado exitosamente",response = JwtDto.class),
            @ApiResponse(code = 400,message = "campos mal puestos || error en el metodo login:",response = Null.class)
    })
    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult){
        JwtDto response = null;
        if(bindingResult.hasErrors()){
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        try{
            Authentication authentication = authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(loginUsuario.getUsuario(),loginUsuario.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generateToken(authentication);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();

            Integer id=  this.usuarioService.
                    findByCorreoElectronicoLike(loginUsuario.getUsuario()).get().getId();

            JwtDto jwdto = new JwtDto(id,jwt,userDetails.getUsername(), userDetails.getAuthorities());
            response = jwdto;
        }catch (Exception exception){
            logger.error("error en el metodo login: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(response);
    }
}
