package sitiapp.gestionrequerimientos.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import javax.validation.constraints.Null;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sitiapp.gestionrequerimientos.model.Respuesta;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.services.RolServices;

@RestController
@RequestMapping("/rol")
@Api("El servicio responde a todas las peticiones relacionadas al modelo Rol")
public class RolRest {

    private final static Logger logger = LoggerFactory.getLogger(RolRest.class);

    @Autowired
    private RolServices rolServices;

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todos los Rol")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Los Rol se han obtenidos exitosamente",responseContainer = "List",response = Rol.class),
            @ApiResponse(code = 400,message = "error en el metodo getRoles:",response = Null.class)
    })
    private ResponseEntity<List<Rol>> getRoles(){
        List<Rol> roles = null;
        try{
            roles = rolServices.findAll();
        }catch (Exception exception){
            logger.error("error en el metodo getRoles: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(roles);
    }
}
