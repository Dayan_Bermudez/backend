package sitiapp.gestionrequerimientos.rest;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sitiapp.gestionrequerimientos.model.*;
import sitiapp.gestionrequerimientos.model.viewModel.UsuarioViewModel;
import sitiapp.gestionrequerimientos.model.createModel.NuevoUsuario;
import sitiapp.gestionrequerimientos.services.UsuarioService;

import javax.validation.Valid;
import javax.validation.constraints.Null;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/usuario")
@Api("El servicio responde a todas las peticiones relacionadas al modelo Usuario")
public class UsuarioRest {

    private final static Logger logger = LoggerFactory.getLogger(UsuarioRest.class);

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todos los Usuarios")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuarios:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuarios(){
        List<UsuarioViewModel> usuariosResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findAll();
            usuariosResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuarios: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuariosResponse);
    }


    @GetMapping(path = "/{id_usuario}",produces = "application/json")
    @ApiOperation(value = "Retorna al Usuario cuyo id coincida con id_usuario")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuario obtenido exitosamente",response = NuevoUsuario.class),
            @ApiResponse(code = 400,message = "No extiste el Usuario con id_usuario || error en el metodo getUsuarioById:",response = Null.class)
    })
    private ResponseEntity<NuevoUsuario> getUsuarioById(@PathVariable("id_usuario") Integer id_usuario){
        NuevoUsuario usuarioResponse = null;
        try{
            Usuario usuarioBuscado = usuarioService.findById(id_usuario).get();
            if(!usuarioBuscado.equals(null)){
                usuarioResponse = new NuevoUsuario(usuarioBuscado.getNombre(),usuarioBuscado.getSegundoNombre(), 
                usuarioBuscado.getApellido(),usuarioBuscado.getSegundoApellido(),usuarioBuscado.getTipoIdentificacion(), 
                usuarioBuscado.getIdentificacion(),usuarioBuscado.getArea(),usuarioBuscado.getRol(),
                usuarioBuscado.getPassword()); 
            }else {
                return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exception){
            logger.error("error en el metodo getUsuarioById: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuarioResponse);
    }


    private Usuario prepararUsuario(NuevoUsuario upUsuario,Usuario usuarioActual){
        String correo = " ";
        upUsuario.setNombre(upUsuario.getNombre().toLowerCase());
        upUsuario.setNombre(upUsuario.getNombre().replaceAll("\\s+",""));
        if(upUsuario.getSegundoNombre()!=null){
            upUsuario.setSegundoNombre(upUsuario.getSegundoNombre().toLowerCase());
            upUsuario.setSegundoNombre(upUsuario.getSegundoNombre().replaceAll("\\s+",""));
        }
        upUsuario.setApellido(upUsuario.getApellido().toLowerCase());
        upUsuario.setApellido(upUsuario.getApellido().replaceAll("\\s+",""));
        if(upUsuario.getSegundoApellido()!=null){
            upUsuario.setSegundoApellido(upUsuario.getSegundoApellido().toLowerCase());
            upUsuario.setSegundoApellido(upUsuario.getSegundoApellido().replaceAll("\\s+",""));
        }
        if(usuarioActual!=null){
            if(upUsuario.getNombre().equals(usuarioActual.getNombre()) &&
                    upUsuario.getApellido().equals(usuarioActual.getApellido())){
                        correo = usuarioActual.getCorreoElectronico();
            }else{
                correo = upUsuario.getNombre()+"."+upUsuario.getApellido()+"@sitiapp.com";
            }
        }else{
            Integer cantidad = usuarioService.countByNombreAndApellido(upUsuario.getNombre(),
                    upUsuario.getApellido());
            if(cantidad !=0){
                String cant = String.valueOf(cantidad);
                correo = upUsuario.getNombre()+"."+upUsuario.getApellido()+"."+cant+"@sitiapp.com";
            }else{
                correo = upUsuario.getNombre()+"."+upUsuario.getApellido()+"@sitiapp.com";
            }
        }
        Usuario usuarioNew = new Usuario(0,upUsuario.getNombre(),upUsuario.getTipoIdentificacion(),
                upUsuario.getIdentificacion(),correo,new Date(),upUsuario.getArea(),1,
                (new Date()),upUsuario.getSegundoNombre(),upUsuario.getApellido(),
                upUsuario.getSegundoApellido(),upUsuario.getRol(),
                passwordEncoder.encode(upUsuario.getPassword()),1);

        return usuarioNew;
    }


    @PostMapping(path = "/nuevo",consumes = {MediaType.APPLICATION_JSON_VALUE},produces = "application/json")
    @ApiOperation(value = "Registra un Usuario")
    @ApiResponses(value = {
            @ApiResponse(code = 201,message = "Usuario registrado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "campos mal puestos || la identificacion ya se encuentra registrada || error en el metodo createUsuarios:",response = Respuesta.class)
    })
    public ResponseEntity<Respuesta> createUsuario(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
        Respuesta respuesta = new Respuesta();
        try{
            if(bindingResult.hasErrors()){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            if(usuarioService.existsByIdentificacion(nuevoUsuario.getIdentificacion())){
                respuesta.mensaje = "la identificacion ya se encuentra registrada";
                respuesta.exito = -1;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            Usuario newUsuario = prepararUsuario(nuevoUsuario,null);
            usuarioService.save(newUsuario);
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo createUsuarios: "+respuesta.mensaje);
            return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(respuesta,HttpStatus.CREATED);
    }


    @PutMapping(value = "{id}",consumes = {MediaType.APPLICATION_JSON_VALUE},produces = "application/json")
    @ApiOperation(value = "Edita un Usuario")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuario editado exitosamente || No cambio ningun campo || campos mal puestos " +
                    "|| la identificacion ya se encuentra registrada || error en el metodo updateUsuarios:",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> updateUsuario(@PathVariable("id") Integer id_usuario,@Valid @RequestBody NuevoUsuario upUsuario, BindingResult bindingResult){
        Respuesta respuesta = new Respuesta();
        try{
            Usuario usuarioActual = usuarioService.findById(id_usuario).get();
            if(usuarioActual.equals(upUsuario)){
                respuesta.exito = -1;
                respuesta.mensaje = "No cambio ningun campo";
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            if(bindingResult.hasErrors() || (upUsuario.getNombre().length()==0 ||
                    upUsuario.getApellido().length()==0)){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            if(!usuarioActual.getIdentificacion().equals(upUsuario.getIdentificacion())){
                if(usuarioService.existsByIdentificacion(upUsuario.getIdentificacion())){
                    respuesta.mensaje = "la identificacion ya se encuentra registrada";
                    respuesta.exito = -1;
                    return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
                }
            }
            Usuario updateUsuario = prepararUsuario(upUsuario,usuarioActual);
            Integer exitoUpdate = usuarioService.updateUsuario(id_usuario,
                    updateUsuario.getNombre(),updateUsuario.getTipoIdentificacion(),
                    updateUsuario.getIdentificacion(),updateUsuario.getCorreoElectronico(),
                    updateUsuario.getArea(),new Date(),updateUsuario.getSegundoNombre(),
                    updateUsuario.getApellido(),updateUsuario.getSegundoApellido(),updateUsuario.getRol());
            if (exitoUpdate != 0){
                respuesta.exito = 1;
                respuesta.mensaje = "Usuario editado exitosamente";
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo updateUsuarios: "+respuesta.mensaje);
            return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }


    @DeleteMapping(value = "{id}",produces = "application/json")
    @ApiOperation(value = "Elimina un Usuario")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuario eliminado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "No se pude eliminar el Usuario",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> deleteUsuario(@PathVariable("id") Integer id_usuario){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoDelete = usuarioService.deleteUsuario(id_usuario);
            if(exitoDelete != 0){
                respuesta.exito = 1;
                respuesta.mensaje = "Usuario eliminado exitosamente";
            }else{
                respuesta.exito = 0;
                respuesta.mensaje = "No se pude eliminar el Usuario";
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo deleteUsuarios: "+respuesta.mensaje);
            return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }


    @GetMapping(path = "/getByNombre/{nombre}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo nombre coincida con el nombre ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosByNombre:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosByNombre(@PathVariable("nombre") String nombre){
        List<UsuarioViewModel> usuariosResponse = null;
        try{
            String nombreBuscado = nombre.toLowerCase();
            List<Usuario> usuarios = usuarioService.findAllByNombreLike(nombreBuscado);
            usuariosResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosByNombre: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuariosResponse);
    }


    @GetMapping(path = "/getByApellido/{apellido}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo apellido coincida con el apellido ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosByApellido:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosByApellido(@PathVariable("apellido") String apellido){
        List<UsuarioViewModel> usuariosResponse = null;
        try{
            String apellidoBuscado = apellido.toLowerCase();
            List<Usuario> usuarios = usuarioService.findAllByApellidoLike(apellidoBuscado);
            usuariosResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosByApellido: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuariosResponse);
    }


    @GetMapping(path = "/getBySegundoNombre/{segundoNombre}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo segundoNombre coincida con el segundoNombre ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosBySegundoNombre:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosBySegundoNombre(@PathVariable("segundoNombre") String segundoNombre){
        List<UsuarioViewModel> usuariosResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findAllBySegundoNombreLike(segundoNombre);
            usuariosResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosBySegundoNombre: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuariosResponse);
    }


    @GetMapping(path = "/getBySegundoApellido/{segundoApellido}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo segundoApellido coincida con el segundoApellido ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosBySegundoApellido:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosBySegundoApellido(@PathVariable("segundoApellido") String segundoApellido){
        List<UsuarioViewModel> usuarioResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findAllBySegundoApellidoLike(segundoApellido);
            usuarioResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosBySegundoApellido: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuarioResponse);
    }


    @GetMapping(path = "/getByIdentificacion/{identificacion}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuya identificacion coincida con la identificacion ingresada")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosByIdentificacion:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosByIdentificacion(@PathVariable("identificacion") Integer identificacion){
        List<UsuarioViewModel> usuarioResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findAllByIdentificacionLike(identificacion.toString());
            usuarioResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosByIdentificacion: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuarioResponse);
    }


    @GetMapping(path = "/getByCorreoElectronico/{correoElectronico}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo correoElectronico coincida con el correoElectronico ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosByCorreoElectronico:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosByCorreoElectronico(@PathVariable("correoElectronico") String correoElectronico){
        List<UsuarioViewModel> usuarioResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findAllByCorreoElectronicoLike(correoElectronico);
            usuarioResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosByCorreoElectronico: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuarioResponse);
    }


    @GetMapping(path = "/getByEstado/{estado}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo estado coincida con el estado ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosByEstado:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosByEstado(@PathVariable("estado") Integer estado){
        List<UsuarioViewModel> usuarioResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findAllByEstado(estado);
            usuarioResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosByEstado: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuarioResponse);
    }


    @GetMapping(path = "/getByTipoId/{tipo}",produces = "application/json")
    @ApiOperation(value = "Obtiene Usuarios cuyo estado coincida con el estado ingresado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Usuarios obtenidos exitosamente",responseContainer = "List",response = UsuarioViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getUsuariosByEstado:",response = Null.class)
    })
    private ResponseEntity<List<UsuarioViewModel>> getUsuariosByTipoId(@PathVariable("tipo") Integer tipo){
        List<UsuarioViewModel> usuarioResponse = null;
        try{
            List<Usuario> usuarios = usuarioService.findByIdTipoIdentificacion(tipo);
            usuarioResponse = devolverUsuariosViewModel(usuarios);
        }catch (Exception exception){
            logger.error("error en el metodo getUsuariosByEstado: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(usuarioResponse);
    }


    private List<UsuarioViewModel> devolverUsuariosViewModel(List<Usuario> usuarios){
        List<UsuarioViewModel> usuarioList = new ArrayList<UsuarioViewModel>();
        for (Usuario usuario: usuarios) {
            UsuarioViewModel usuarioViewModel = new UsuarioViewModel(usuario.getId(),usuario.getNombre(),
                    usuario.getSegundoNombre(),usuario.getApellido(),usuario.getSegundoApellido(),
                    usuario.getTipoIdentificacion(),usuario.getIdentificacion(),usuario.getArea(),
                    usuario.getRol(),usuario.getCorreoElectronico(),usuario.getFechaDeIngreso());
            usuarioList.add(usuarioViewModel);
        }
        return usuarioList;
    }
}
