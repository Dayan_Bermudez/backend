package sitiapp.gestionrequerimientos.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sitiapp.gestionrequerimientos.model.EstadoRequerimiento;
import sitiapp.gestionrequerimientos.model.Requerimiento;
import sitiapp.gestionrequerimientos.model.Respuesta;
import sitiapp.gestionrequerimientos.model.Usuario;
import sitiapp.gestionrequerimientos.model.createModel.NuevoRequerimiento;
import sitiapp.gestionrequerimientos.model.viewModel.RequerimientoViewModel;
import sitiapp.gestionrequerimientos.model.viewModel.RequerimientosDeUsuariosViewModel;
import sitiapp.gestionrequerimientos.services.RequerimientoService;
import sitiapp.gestionrequerimientos.services.RequerimientosDeUsuarioService;

import javax.validation.Valid;
import javax.validation.constraints.Null;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/requerimiento")
@Api("El servicio responde a todas las peticiones relacionadas al modelo Requrimiento")
public class RequerimientoRest {

    private final static Logger logger = LoggerFactory.getLogger(RequerimientoRest.class);

    @Autowired
    private RequerimientoService requerimientoService;

    @Autowired
    private RequerimientosDeUsuarioService requerimientosDeUsuarioService;

    @GetMapping(produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todos los Requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Los Requerimiento se han obtenido exitosamente",responseContainer = "List",response = RequerimientoViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getRequerimientos:",response = Null.class)
    })
    private ResponseEntity<List<RequerimientoViewModel>> getRequerimientos(){
        List<RequerimientoViewModel> requerimientos = null;
        try{
            List<Requerimiento> requerimientosList = requerimientoService.findAll();
            requerimientos = prepararRequerimientos(requerimientosList);
        }catch (Exception exception){
            logger.error("error en el metodo getRequerimientos: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(requerimientos);
    }


    @GetMapping(path = "/empleado/{id_empleado}",produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todos los Requerimiento asignados al empleado con id_empleado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Los Requerimiento se han obtenido exitosamente",responseContainer = "List",response = RequerimientoViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getRequerimientosByEmpleado:",response = Null.class)
    })
    private ResponseEntity<List<RequerimientoViewModel>>getRequerimientosByEmpleado(@PathVariable("id_empleado") Integer id_empledo){
        List<RequerimientoViewModel> requerimientos = null;
        try{
            List<Requerimiento> requerimientosList = requerimientoService.findAllByEmpleadoId(id_empledo);
           requerimientos = prepararRequerimientos(requerimientosList);
        }catch (Exception exception){
            logger.error("error en el metodo getRequerimientosByEmpleado: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(requerimientos);
    }


    @GetMapping(path = "/{id_requerimiento}",produces = "application/json")
    @ApiOperation(value = "Retorna el Requerimiento cuyo id coincida con id_requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "EL Requerimiento se ha obtenido exitosamente",response = RequerimientoViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getRequerimientosById:",response = Null.class)
    })
    private ResponseEntity<RequerimientoViewModel> getRequerimientosById(@PathVariable("id_requerimiento") Integer id_requerimiento){
        RequerimientoViewModel requerimientoResponse = null;
        try{
            Optional<Requerimiento> requerimiento = requerimientoService.findById(id_requerimiento);
            if(requerimiento.equals(null)){
                return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
            }else{
                List<Requerimiento> requerimientoList = new ArrayList<>();
                requerimientoList.add(requerimiento.get());
                requerimientoResponse = prepararRequerimientos(requerimientoList).get(0);
            }
        }catch (Exception exception){
            logger.error("error en el metodo getRequerimientosById: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(requerimientoResponse);
    }


    private List<RequerimientoViewModel> prepararRequerimientos(List<Requerimiento> requerimientos){
        List<RequerimientoViewModel> requerimientoViewModelList = new ArrayList<RequerimientoViewModel>();
        for (Requerimiento reque:requerimientos) {
            RequerimientoViewModel requerimientoViewModel = new RequerimientoViewModel(reque.getId(),
                    reque.getDescripcion(),reque.getEmpleado(),reque.getFechaDeRegistro(),reque.getResponsable(),
                    reque.getEstado());
            requerimientoViewModelList.add(requerimientoViewModel);
            requerimientoViewModel = null;
        }
        return requerimientoViewModelList;
    }

    @GetMapping(path = "/usuario",produces = "application/json")
    @ApiOperation(value = "Retorna la lista de todos los RequerimientoDeUsuario")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Los RequerimientoDeUsuario se han obtenido exitosamente",responseContainer = "List",response = RequerimientoViewModel.class),
            @ApiResponse(code = 400,message = "error en el metodo getRequerimientosDeUsuario:",response = Null.class)
    })
    private ResponseEntity<List<RequerimientosDeUsuariosViewModel>> getRequerimientosDeUsuario(){
        List<RequerimientosDeUsuariosViewModel> requerimienosDeUsuario = null;
        try{
            requerimienosDeUsuario = requerimientosDeUsuarioService.findAll();
        }catch (Exception exception){
            logger.error("error en el metodo getRequerimientosDeUsuario: "+exception.getMessage());
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(requerimienosDeUsuario);
    }


    @PostMapping(produces = "application/json")
    @ApiOperation(value = "Registra un Requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "El Requerimiento se ha registrado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "campos mal puestos || error en el metodo createRequerimiento:",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> createRequerimiento(@Valid @RequestBody NuevoRequerimiento newRequerimiento, BindingResult bindingResult){
        Respuesta respuesta = new Respuesta();
        try{
            if(bindingResult.hasErrors()){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            Usuario empleado = new Usuario();
            Usuario responsable = new Usuario();
            empleado.setId(newRequerimiento.getEmpleado_id());
            responsable.setId(newRequerimiento.getResponsable_id());
            EstadoRequerimiento estado = new EstadoRequerimiento();
            estado.setId(2);

            Requerimiento nuevoRequerimiento = new Requerimiento(0,newRequerimiento.getDescripcion(),empleado,
                    new Date(),responsable,1,estado);

            respuesta.exito = 1;
            respuesta.mensaje ="El Requerimiento se ha registrado exitosamente";
            requerimientoService.save(nuevoRequerimiento);
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo createRequerimiento: "+respuesta.mensaje);
            return new ResponseEntity(respuesta, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }


    @PutMapping(path = "{id_requerimiento}",produces = "application/json")
    @ApiOperation(value = "Edita un Requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "El Requerimiento se ha editado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "campos mal puestos || error en el metodo updateRequerimiento:",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> updateRequerimiento(@PathVariable("id_requerimiento") Integer id_requerimiento,@Valid @RequestBody NuevoRequerimiento newRequerimiento, BindingResult bindingResult){
        Respuesta respuesta = new Respuesta();
        try{
            if(bindingResult.hasErrors()){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            Usuario empleado = new Usuario();
            empleado.setId(newRequerimiento.getEmpleado_id());
            EstadoRequerimiento estado = new EstadoRequerimiento();
            estado.setId(2);

            requerimientoService.updateRequerimiento(id_requerimiento,newRequerimiento.getDescripcion(),
                    empleado,estado);
            respuesta.exito = 1;
            respuesta.mensaje ="El Requerimiento se ha editado exitosamente";
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo updateRequerimiento: "+respuesta.mensaje);
            return new ResponseEntity(respuesta, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }


    @PutMapping(path = "/updateEstado/{id_requerimiento}",produces = "application/json")
    @ApiOperation(value = "Edita el estado de un Requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "El estado del Requerimiento se ha editado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "error en el metodo updateEstadoDelRequerimiento:",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> updateEstadoDelRequerimiento(@PathVariable("id_requerimiento") Integer id_requerimiento,
                                                                   @RequestBody Integer estado_id){
        Respuesta respuesta = new Respuesta();
        try{
            if(id_requerimiento<=0 && estado_id<=0){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            requerimientoService.updateEstadoDelRequerimiento(id_requerimiento,estado_id);
            respuesta.exito = 1;
            respuesta.mensaje ="El estado del Requerimiento se ha editado exitosamente";
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo updateEstadoDelRequerimiento: "+respuesta.mensaje);
            return new ResponseEntity(respuesta, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }


    @PutMapping(path = "/updateEmpleado/{id_requerimiento}",produces = "application/json")
    @ApiOperation(value = "Edita el empleado de un Requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "El empleado del Requerimiento se ha editado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "error en el metodo updateEmpleadoDelRequerimiento:",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> updateEmpleadoDelRequerimiento(@PathVariable("id_requerimiento") Integer id_requerimiento,
                                                                   @RequestBody Integer empleado_id){
        Respuesta respuesta = new Respuesta();
        try{
            if(id_requerimiento<=0 && empleado_id<=0){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return new ResponseEntity(respuesta,HttpStatus.BAD_REQUEST);
            }
            requerimientoService.updateEmpleadoDelRequerimiento(id_requerimiento,empleado_id);
            respuesta.exito = 1;
            respuesta.mensaje ="El empleado del Requerimiento se ha editado exitosamente";
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo updateEmpleadoDelRequerimiento: "+respuesta.mensaje);
            return new ResponseEntity(respuesta, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }


    @DeleteMapping(path = "/{id_requerimiento}",produces = "application/json")
    @ApiOperation(value = "Elimina un Requerimiento")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "El Requerimiento se ha eliminado exitosamente",response = Respuesta.class),
            @ApiResponse(code = 400,message = "error en el metodo deleteRequerimiento:",response = Respuesta.class)
    })
    private ResponseEntity<Respuesta> deleteRequerimiento(@PathVariable("id_requerimiento") Integer id_requerimiento){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoDelete = requerimientoService.deleteRequerimiento(id_requerimiento);
            if(exitoDelete!=0){
                respuesta.exito = 1;
                respuesta.mensaje ="El Requerimiento se ha eliminado exitosamente";
            }else{
                respuesta.exito = 0;
                respuesta.mensaje = "No se ha eliminado el Requerimiento con id_requerimiento";
                return new ResponseEntity(respuesta, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
            logger.error("error en el metodo deleteRequerimiento: "+respuesta.mensaje);
            return new ResponseEntity(respuesta, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(respuesta);
    }
}
