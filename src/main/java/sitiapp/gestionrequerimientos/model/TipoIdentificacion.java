package sitiapp.gestionrequerimientos.model;

import javax.persistence.*;

@Table(name = "tipo_identificacion")
@Entity
public class TipoIdentificacion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "abreviatura", length = 50)
    private String abreviatura;

    @Column(name = "descripcion", length = 100)
    private String descripcion;

    public TipoIdentificacion() {
    }

    public TipoIdentificacion(Integer id, String abreviatura, String descripcion) {
        this.id = id;
        this.abreviatura = abreviatura;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}