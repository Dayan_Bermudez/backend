package sitiapp.gestionrequerimientos.model;

import javax.persistence.*;
import java.util.Date;

@Table(name = "usuario")
@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "nombre", length = 100)
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "id_tipo_identificacion")
    private TipoIdentificacion tipoIdentificacion;

    @Column(name = "identificacion")
    private Integer identificacion;

    @Column(name = "correo_electronico", length = 300)
    private String correoElectronico;

    @Column(name = "fecha_de_ingreso")
    private Date fechaDeIngreso;

    @ManyToOne
    @JoinColumn(name = "id_area")
    private Area area;

    @Column(name = "estado")
    private Integer estado;

    @Column(name = "fecha_de_edicion")
    private Date fechaDeEdicion;

    @Column(name = "segundo_nombre", length = 100)
    private String segundoNombre;

    @Column(name = "apellido", length = 100)
    private String apellido;

    @Column(name = "segundo_apellido", length = 100)
    private String segundoApellido;

    @ManyToOne
    @JoinColumn(name = "id_rol")
    private Rol rol;

    @Column(name = "password", length = 300)
    private String password;

    @Column(name = "habilitado")
    private Integer habilitado;

    public Usuario() {
    }

    public Usuario(Integer id, String nombre, TipoIdentificacion idTipoIdentificacion, Integer identificacion, String correoElectronico, Date fechaDeIngreso, Area idArea, Integer estado, Date fechaDeEdicion, String segundoNombre, String apellido, String segundoApellido, Rol idRol, String password, Integer habilitado) {
        this.id = id;
        this.nombre = nombre;
        this.tipoIdentificacion = idTipoIdentificacion;
        this.identificacion = identificacion;
        this.correoElectronico = correoElectronico;
        this.fechaDeIngreso = fechaDeIngreso;
        this.area = idArea;
        this.estado = estado;
        this.fechaDeEdicion = fechaDeEdicion;
        this.segundoNombre = segundoNombre;
        this.apellido = apellido;
        this.segundoApellido = segundoApellido;
        this.rol = idRol;
        this.password = password;
        this.habilitado = habilitado;
    }

    public Integer getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Integer habilitado) {
        this.habilitado = habilitado;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol idRol) {
        this.rol = idRol;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public Date getFechaDeEdicion() {
        return fechaDeEdicion;
    }

    public void setFechaDeEdicion(Date fechaDeEdicion) {
        this.fechaDeEdicion = fechaDeEdicion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area idArea) {
        this.area = idArea;
    }

    public Date getFechaDeIngreso() {
        return fechaDeIngreso;
    }

    public void setFechaDeIngreso(Date fechaDeIngreso) {
        this.fechaDeIngreso = fechaDeIngreso;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Integer getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(Integer identificacion) {
        this.identificacion = identificacion;
    }

    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion idTipoIdentificacion) {
        this.tipoIdentificacion = idTipoIdentificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}