package sitiapp.gestionrequerimientos.model;

import javax.persistence.*;
import java.util.Date;

@Table(name = "requerimiento")
@Entity
public class Requerimiento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "descripcion", length = 500)
    private String descripcion;

    @ManyToOne
    @JoinColumn(name = "empleado_id")
    private Usuario empleado;

    @Column(name = "fecha_de_registro")
    private Date fechaDeRegistro;

    @ManyToOne
    @JoinColumn(name = "responsable_id")
    private Usuario responsable;

    @Column(name = "habilitado")
    private Integer habilitado;

    @ManyToOne
    @JoinColumn(name = "estado_id")
    private EstadoRequerimiento estado;

    public Requerimiento() {
    }

    public Requerimiento(Integer id, String descripcion, Usuario empleado, Date fechaDeRegistro, Usuario responsable, Integer habilitado, EstadoRequerimiento estado) {
        this.id = id;
        this.descripcion = descripcion;
        this.empleado = empleado;
        this.fechaDeRegistro = fechaDeRegistro;
        this.responsable = responsable;
        this.habilitado = habilitado;
        this.estado = estado;
    }

    public EstadoRequerimiento getEstado() {
        return estado;
    }

    public void setEstado(EstadoRequerimiento estado) {
        this.estado = estado;
    }

    public Integer getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Integer habilitado) {
        this.habilitado = habilitado;
    }

    public Usuario getResponsable() {
        return responsable;
    }

    public void setResponsable(Usuario responsable) {
        this.responsable = responsable;
    }

    public Date getFechaDeRegistro() {
        return fechaDeRegistro;
    }

    public void setFechaDeRegistro(Date fechaDeRegistro) {
        this.fechaDeRegistro = fechaDeRegistro;
    }

    public Usuario getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Usuario empleado) {
        this.empleado = empleado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}