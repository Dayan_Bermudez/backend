package sitiapp.gestionrequerimientos.model.viewModel;

import sitiapp.gestionrequerimientos.model.EstadoRequerimiento;
import sitiapp.gestionrequerimientos.model.Usuario;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RequerimientoViewModel {
    private Integer id;
    private String descripcion;
    private UsuarioViewModel empleado;
    private String fechaDeRegistro;
    private UsuarioViewModel responsable;
    private String estado;

    public RequerimientoViewModel() {
    }

    public RequerimientoViewModel(Integer id, String descripcion, Usuario empleado, Date fechaDeRegistro, Usuario responsable, EstadoRequerimiento estado) {
        this.id = id;
        this.descripcion = descripcion;
        this.empleado = new UsuarioViewModel();
        this.responsable = new UsuarioViewModel();

        this.empleado.setId(empleado.getId());
        this.empleado.setNombre(empleado.getNombre());
        this.empleado.setSegundoNombre(empleado.getSegundoNombre());
        this.empleado.setApellido(empleado.getApellido());
        this.empleado.setSegundoApellido(empleado.getSegundoApellido());
        this.empleado.setArea(empleado.getArea());
        this.empleado.setCorreoElectronico(empleado.getCorreoElectronico());
        this.empleado.setRol(empleado.getRol());
        this.empleado.setTipoIdentificacion(empleado.getTipoIdentificacion());
        this.empleado.setIdentificacion(empleado.getIdentificacion());

        this.responsable.setId(responsable.getId());
        this.responsable.setNombre(responsable.getNombre());
        this.responsable.setSegundoNombre(responsable.getSegundoNombre());
        this.responsable.setApellido(responsable.getApellido());
        this.responsable.setSegundoApellido(responsable.getSegundoApellido());
        this.responsable.setArea(responsable.getArea());
        this.responsable.setCorreoElectronico(responsable.getCorreoElectronico());
        this.responsable.setRol(responsable.getRol());
        this.responsable.setTipoIdentificacion(responsable.getTipoIdentificacion());
        this.responsable.setIdentificacion(responsable.getIdentificacion());

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.fechaDeRegistro = format.format(fechaDeRegistro);
        this.empleado.setFechaIngreso(format.format(empleado.getFechaDeIngreso()));
        this.responsable.setFechaIngreso(format.format(responsable.getFechaDeIngreso()));
        this.estado = estado.getNombre();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public UsuarioViewModel getEmpleado() {
        return empleado;
    }

    public void setEmpleado(UsuarioViewModel empleado) {
        this.empleado = empleado;
    }

    public String getFechaDeRegistro() {
        return fechaDeRegistro;
    }

    public void setFechaDeRegistro(String fechaDeRegistro) {
        this.fechaDeRegistro = fechaDeRegistro;
    }

    public UsuarioViewModel getResponsable() {
        return responsable;
    }

    public void setResponsable(UsuarioViewModel responsable) {
        this.responsable = responsable;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
