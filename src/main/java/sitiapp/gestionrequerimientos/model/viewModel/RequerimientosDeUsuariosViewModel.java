package sitiapp.gestionrequerimientos.model.viewModel;

import sitiapp.gestionrequerimientos.model.Usuario;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@SqlResultSetMapping(
        name = "requerimientos",
        entities = {
                @EntityResult(
                        entityClass = RequerimientosDeUsuariosViewModel.class,
                        fields = {
                                @FieldResult(name = "id", column = "id"),
                                @FieldResult(name = "name", column = "nombre_completo"),
                                @FieldResult(name = "requerimientosAsignados", column = "asignados"),
                                @FieldResult(name = "requerimientosRealizados", column = "realizados"),
                                @FieldResult(name = "requerimientosPendientes", column = "pendientes")
                        }
                )
        }
)
@NamedNativeQuery(
        name = "all.requerimientos",
        query = "select us.id,concat(us.nombre,' ',us.segundo_nombre,' ',us.apellido,' ',us.segundo_apellido) as nombre_completo, " +
                "       count(r) as asignados, count(case when r.estado_id=2 then 1 end) as pendientes, " +
                "       count(case when r.estado_id=1 then 1 end) as realizados " +
                "from usuario us " +
                "join requerimiento r on us.id = r.empleado_id " +
                "where us.habilitado<>0 " +
                "group by us.id,nombre_completo;",
        resultSetMapping = "requerimientos"
)
public class RequerimientosDeUsuariosViewModel implements Serializable {
    @Id
    private Integer id;
    private String name;
    private Integer requerimientosAsignados;
    private Integer requerimientosRealizados;
    private Integer requerimientosPendientes;

    public RequerimientosDeUsuariosViewModel() {
    }

    public RequerimientosDeUsuariosViewModel(Integer Id,Usuario usuario, Long requerimientosAsignados, Long requerimientosRealizados, Long requerimientosPendientes) {
        this.id = id;
        this.name =usuario.getNombre();
        if(!usuario.getSegundoNombre().equals(null)){
            this.name += " "+usuario.getSegundoNombre();
        }
        this.name += " "+usuario.getApellido();
        if(!usuario.getSegundoApellido().equals(null)){
            this.name += " "+usuario.getSegundoApellido();
        }
        this.requerimientosAsignados = requerimientosAsignados.intValue();
        this.requerimientosRealizados = requerimientosRealizados.intValue();
        this.requerimientosPendientes = requerimientosPendientes.intValue();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return name;
    }

    public void setUsuario(Usuario usuario) {
        this.name= usuario.getNombre()+" "+usuario.getSegundoNombre()+" "+usuario.getApellido()+" "+usuario.getSegundoApellido();
    }

    public Integer getRequerimientosAsignados() {
        return requerimientosAsignados;
    }

    public void setRequerimientosAsignados(Integer requerimientosAsignados) {
        this.requerimientosAsignados = requerimientosAsignados;
    }

    public Integer getRequerimientosRealizados() {
        return requerimientosRealizados;
    }

    public void setRequerimientosRealizados(Integer requerimientosRealizados) {
        this.requerimientosRealizados = requerimientosRealizados;
    }

    public Integer getRequerimientosPendientes() {
        return requerimientosPendientes;
    }

    public void setRequerimientosPendientes(Integer requerimientosPendientes) {
        this.requerimientosPendientes = requerimientosPendientes;
    }
}
