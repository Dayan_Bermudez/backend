package sitiapp.gestionrequerimientos.model.viewModel;

import sitiapp.gestionrequerimientos.model.Area;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.model.TipoIdentificacion;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UsuarioViewModel {
    private Integer id;
    private String nombre;
    private String segundoNombre;
    private String apellido;
    private String segundoApellido;
    private String tipoIdentificacion;
    private Integer identificacion;
    private String area;
    private String rol;
    private String correoElectronico;
    private String fechaIngreso;

    public UsuarioViewModel() {
    }

    public UsuarioViewModel(Integer id, String nombre, String segundoNombre, String apellido, String segundoApellido, TipoIdentificacion tipoIdentificacion, Integer identificacion, Area area, Rol rol, String correoElectronico, Date fecha) {
        this.id = id;
        this.nombre = nombre;
        this.segundoNombre = segundoNombre;
        this.apellido = apellido;
        this.segundoApellido = segundoApellido;
        this.tipoIdentificacion = tipoIdentificacion.getDescripcion()+"("+tipoIdentificacion.getAbreviatura()+")";
        this.identificacion = identificacion;
        this.area = area.getNombre();
        this.rol = rol.getNombre();
        this.correoElectronico = correoElectronico;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.fechaIngreso = format.format(fecha);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion.getDescripcion()+"("+tipoIdentificacion.getAbreviatura()+")";;
    }

    public Integer getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(Integer identificacion) {
        this.identificacion = identificacion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area.getNombre();
    }

    public String getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol.getNombre();
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}
