package sitiapp.gestionrequerimientos.model.createModel;

import sitiapp.gestionrequerimientos.model.Area;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.model.TipoIdentificacion;

import javax.validation.constraints.NotNull;

public class NuevoUsuario {
    @NotNull
    private String nombre;
    private String segundoNombre;
    @NotNull
    private String apellido;
    private String segundoApellido;
    @NotNull
    private TipoIdentificacion tipoIdentificacion;
    @NotNull
    private Integer identificacion;
    @NotNull
    private Area area;
    @NotNull
    private Rol rol;
    private String password;

    public NuevoUsuario() {
    }

    public NuevoUsuario(String nombre, String segundoNombre, String apellido, String segundoApellido, TipoIdentificacion tipoIdentificacion, Integer identificacion, Area area, Rol rol,String password) {
        this.nombre = nombre;
        this.segundoNombre = segundoNombre;
        this.apellido = apellido;
        this.segundoApellido = segundoApellido;
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
        this.area = area;
        this.rol = rol;
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public Integer getIdentificacion() {
        return this.identificacion;
    }

    public void setIdentificacion(Integer identificacion) {
        this.identificacion = identificacion;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
