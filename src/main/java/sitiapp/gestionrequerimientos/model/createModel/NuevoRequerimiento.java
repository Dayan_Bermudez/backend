package sitiapp.gestionrequerimientos.model.createModel;

import sitiapp.gestionrequerimientos.model.EstadoRequerimiento;
import sitiapp.gestionrequerimientos.model.viewModel.UsuarioViewModel;

public class NuevoRequerimiento {
    private String descripcion;
    private Integer empleado_id;
    private Integer responsable_id;
    private Integer estado_id;

    public NuevoRequerimiento() {
    }

    public NuevoRequerimiento(String descripcion, Integer empleado_id, Integer responsable_id,Integer estado_id) {
        this.descripcion = descripcion;
        this.empleado_id = empleado_id;
        this.responsable_id = responsable_id;
        this.estado_id = estado_id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEmpleado_id() {
        return empleado_id;
    }

    public void setEmpleado_id(Integer empleado_id) {
        this.empleado_id = empleado_id;
    }

    public Integer getResponsable_id() {
        return responsable_id;
    }

    public void setResponsable_id(Integer responsable_id) {
        this.responsable_id = responsable_id;
    }

    public Integer getEstado_id() {
        return estado_id;
    }

    public void setEstado_id(Integer estado_id) {
        this.estado_id = estado_id;
    }
}
