package sitiapp.gestionrequerimientos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sitiapp.gestionrequerimientos.model.Rol;

@Repository
public interface RolRepository extends JpaRepository<Rol,Integer> {
}
