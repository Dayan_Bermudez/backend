package sitiapp.gestionrequerimientos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.gestionrequerimientos.model.Area;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.model.TipoIdentificacion;
import sitiapp.gestionrequerimientos.model.Usuario;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Integer> {

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 " +
            "order by us.id")
    List<Usuario> findAll();

    Optional<Usuario> findById(Integer id);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 and us.nombre like %:nombre% " +
            "order by us.id")
    List<Usuario> findAllByNombreLike(@Param("nombre") String nombre);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 and us.segundoNombre like %:segundoNombre% " +
            "order by us.id")
    List<Usuario> findAllBySegundoNombreLike(@Param("segundoNombre") String segundoNombre);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 and us.apellido like %:apellido% " +
            "order by us.id")
    List<Usuario> findAllByApellidoLike(@Param("apellido") String apellido);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 and us.segundoApellido like %:segundoApellido% " +
            "order by us.id")
    List<Usuario> findAllBySegundoApellidoLike(@Param("segundoApellido") String segundoApellido);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado)" +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 and concat(us.identificacion,' ') like %:identificacion% " +
            "order by us.id")
    List<Usuario> findAllByIdentificacionLike(@Param("identificacion") String identificacion);

    Optional<Usuario> findByCorreoElectronicoLike(String correo);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado <> 0 and us.correoElectronico like %:correo% " +
            "order by us.id")
    List<Usuario> findAllByCorreoElectronicoLike(@Param("correo") String correo);


    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us join Rol r on r.id=us.rol.id " +
            "where us.habilitado=:estado " +
            "order by us.id")
    List<Usuario> findAllByEstado(@Param("estado") Integer estado);

    Integer countByNombreAndApellido(String nombre,String apellido);

    @Query("select new Usuario (us.id,us.nombre,us.tipoIdentificacion, " +
            "us.identificacion,us.correoElectronico,us.fechaDeIngreso,us.area, " +
            "us.estado,us.fechaDeEdicion,us.segundoNombre,us.apellido, " +
            "us.segundoApellido,us.rol,us.password,us.habilitado) " +
            "from Usuario us left join Rol r on r.id= us.rol.id " +
            "where us.habilitado <> 0 and us.tipoIdentificacion.id=:id_tipo " +
            "order by us.id")
    List<Usuario> findByIdTipoIdentificacion(@Param("id_tipo") Integer id_tipo);

    boolean existsByIdentificacion(Integer identificacion);

    @Transactional
    @Modifying
    @Query("UPDATE Usuario us SET us.nombre=:nombre, us.tipoIdentificacion=:tipoIdentificacion, " +
            "us.identificacion=:identificacion, us.correoElectronico=:correoElectronico, " +
            "us.area=:area, us.fechaDeEdicion=:fechaDeEdicion,us.segundoNombre=:segundoNombre" +
            ",us.apellido=:apellido, us.segundoApellido=:segundoApellido,us.rol=:rol" +
            " WHERE us.id=:id")
    Integer updateUsuario(@Param("id") Integer id,@Param("nombre") String nombre,@Param("tipoIdentificacion") TipoIdentificacion tipoIdentificacion,@Param("identificacion") Integer identificacion,@Param("correoElectronico") String correoElectronico,@Param("area") Area area,@Param("fechaDeEdicion") Date fechaDeEdicion,@Param("segundoNombre") String segundoNombre,@Param("apellido") String apellido,@Param("segundoApellido") String segundoApellido,@Param("rol") Rol rol);

    @Transactional
    @Modifying
    @Query("UPDATE Usuario us SET us.habilitado=0 where us.id=:id_usuario")
    Integer deleteUsuario(@Param("id_usuario") Integer id_usuario);

    @Query("select max(us.id) from Usuario us")
    Integer getMaxId();
}
