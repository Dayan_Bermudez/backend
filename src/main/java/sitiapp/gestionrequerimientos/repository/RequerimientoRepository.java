package sitiapp.gestionrequerimientos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.gestionrequerimientos.model.EstadoRequerimiento;
import sitiapp.gestionrequerimientos.model.Requerimiento;
import sitiapp.gestionrequerimientos.model.Usuario;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface RequerimientoRepository extends JpaRepository<Requerimiento,Integer> {

    @Query("select new Requerimiento (rq.id,rq.descripcion,rq.empleado, " +
            "rq.fechaDeRegistro,rq.responsable,rq.habilitado,rq.estado) " +
            "from Requerimiento rq " +
            "join Usuario empleado on empleado.id=rq.empleado.id " +
            "join Usuario responsable on responsable.id=rq.responsable.id " +
            "join EstadoRequerimiento estado on estado.id=rq.estado.id " +
            "where rq.habilitado <> 0 " +
            "order by rq.id desc")
    List<Requerimiento> findAll();

    @Query("select new Requerimiento (rq.id,rq.descripcion,rq.empleado, " +
            "rq.fechaDeRegistro,rq.responsable,rq.habilitado,rq.estado) " +
            "from Requerimiento rq " +
            "join Usuario empleado on empleado.id=rq.empleado.id " +
            "join Usuario responsable on responsable.id=rq.responsable.id " +
            "join EstadoRequerimiento estado on estado.id=rq.estado.id " +
            "where rq.habilitado <> 0 and rq.id=:id_requerimiento " +
            "order by rq.id")
    Optional<Requerimiento> findById(@Param("id_requerimiento") Integer id_requerimiento);

    @Query("select new Requerimiento (rq.id,rq.descripcion,rq.empleado, " +
            "rq.fechaDeRegistro,rq.responsable,rq.habilitado,rq.estado) " +
            "from Requerimiento rq " +
            "join Usuario empleado on empleado.id=rq.empleado.id " +
            "join Usuario responsable on responsable.id=rq.responsable.id " +
            "join EstadoRequerimiento estado on estado.id=rq.estado.id " +
            "where rq.habilitado <> 0 and rq.empleado.id=:id_empleado " +
            "order by rq.id")
    List<Requerimiento> findAllByEmpleadoId(@Param("id_empleado") Integer id_empleado);

    @Transactional
    @Modifying
    @Query("UPDATE Requerimiento rq SET rq.descripcion=:descripcion,rq.estado=:estado,rq.empleado=:empleado" +
            " where rq.id=:id_requerimiento")
    Integer updateRequerimiento(@Param("id_requerimiento") Integer id_requerimiento,
                                @Param("descripcion") String descripcion,
                                @Param("empleado") Usuario empleado,
                                @Param("estado") EstadoRequerimiento estado);

    @Transactional
    @Modifying
    @Query("UPDATE Requerimiento rq SET rq.estado.id=:estado where rq.id=:id_requerimiento")
    Integer updateEstadoDelRequerimiento(@Param("id_requerimiento") Integer id_requerimiento,
                         @Param("estado") Integer estado);

    @Transactional
    @Modifying
    @Query("UPDATE Requerimiento rq SET rq.empleado.id=:empleado where rq.id=:id_requerimiento")
    Integer updateEmpleadoDelRequerimiento(@Param("id_requerimiento") Integer id_requerimiento,
                                           @Param("empleado") Integer empleado);

    @Transactional
    @Modifying
    @Query("UPDATE Requerimiento rq SET rq.habilitado=0 where rq.id=:id_requerimiento")
    Integer deleteRequerimiento(@Param("id_requerimiento") Integer id_requerimiento);
}
