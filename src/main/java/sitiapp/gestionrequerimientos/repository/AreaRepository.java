package sitiapp.gestionrequerimientos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sitiapp.gestionrequerimientos.model.Area;

@Repository
public interface AreaRepository extends JpaRepository<Area,Integer> {
}
