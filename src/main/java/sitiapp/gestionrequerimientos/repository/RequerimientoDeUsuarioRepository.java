package sitiapp.gestionrequerimientos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sitiapp.gestionrequerimientos.model.viewModel.RequerimientosDeUsuariosViewModel;

import java.util.List;

@Repository
public interface RequerimientoDeUsuarioRepository extends JpaRepository<RequerimientosDeUsuariosViewModel,Integer> {

    @Override
    @Query(name = "all.requerimientos")
    List<RequerimientosDeUsuariosViewModel> findAll();
}
