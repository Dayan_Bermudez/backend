package sitiapp.gestionrequerimientos.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sitiapp.gestionrequerimientos.model.Rol;
import sitiapp.gestionrequerimientos.model.Usuario;
import sitiapp.gestionrequerimientos.security.entity.UsuarioPrincipal;
import sitiapp.gestionrequerimientos.security.entity.UsuarioSecurity;
import sitiapp.gestionrequerimientos.services.UsuarioService;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UsuarioService userService;

    @Override
    public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
        Usuario usuario = userService.findByCorreoElectronicoLike(user).get();
        UsuarioSecurity usuarioSecurity = new UsuarioSecurity();
        usuarioSecurity.setNombre(usuario.getNombre());
        usuarioSecurity.setApellido(usuario.getApellido());
        usuarioSecurity.setUsuario(usuario.getCorreoElectronico());
        usuarioSecurity.setContraseña(usuario.getPassword());
        Set<Rol> perfils = new HashSet<>();
        perfils.add(usuario.getRol());
        usuarioSecurity.setPerfiles(perfils);
        return UsuarioPrincipal.build(usuarioSecurity);
    }
}
