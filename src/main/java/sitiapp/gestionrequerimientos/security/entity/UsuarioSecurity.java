package sitiapp.gestionrequerimientos.security.entity;

import sitiapp.gestionrequerimientos.model.Rol;

import java.util.Set;

public class UsuarioSecurity{
    private String nombre;
    private String apellido;
    private String usuario;
    private String contraseña;
    private Set<Rol> roles;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Set<Rol> getPerfiles() {
        return roles;
    }

    public void setPerfiles(Set<Rol> perfiles) {
        this.roles = perfiles;
    }
}
