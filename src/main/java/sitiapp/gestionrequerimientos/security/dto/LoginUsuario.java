package sitiapp.gestionrequerimientos.security.dto;

import com.sun.istack.NotNull;

public class LoginUsuario {
    @NotNull
    private String usuario;
    @NotNull
    private String password;

    public LoginUsuario() {
    }

    public LoginUsuario(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
